import axios from "axios";

export const URLS = "https://staging-am.awalmula.co.id/rest/default/V1";
export const FIND =
  "https://staging-am.awalmula.co.id/rest/default/V1/products?searchCriteria[pageSize]=10";
export const TOKEN =
  " https://staging-am.awalmula.co.id/rest/V1/integration/admin/token";

export function getCategory() {
  return new Promise((resolve, reject) => {
    try {
      axios
        .get(URLS + "/categories")
        .then((response) => response.data)
        .then((res) => {
          resolve(res.children_data);
        });
    } catch (error) {
      reject(error);
    }
  });
}

export function getProduct(id) {
  return new Promise((resolve, reject) => {
    try {
      axios
        .get(URLS + "/categories/" + id + "/products")
        .then((response) => response.data)
        .then((res) => {
          resolve(res);
        });
    } catch (error) {
      reject(error);
    }
  });
}

export function getProductSearchs() {
  return new Promise((resolve, reject) => {
    axios
      .get(FIND)
      .then((res) => {
        resolve(res.data.items);
      })
      .catch((err) => {
        console.log(err);
      });
  });
}

export function getToken() {
  fetch(TOKEN, {
    method: "POST",
    body: JSON.stringify({
      username: "admin",
      password: "admin123",
    }),
  });
}
