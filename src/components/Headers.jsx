import { Navbar, Container, Button } from "react-bootstrap";

function Headers() {
  return (
    <Navbar>
      <Container fluid>
        <Navbar.Brand href="#home" className="brand">
          #fanscatalog
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            <Button variant="outline-primary">login</Button>
          </Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Headers;
