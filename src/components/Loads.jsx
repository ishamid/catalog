import { Row, Col } from "react-bootstrap";

const Loads = ({ tiles, rules }) => {
  let loop = [];
  for (let i = 0; i < tiles; i++) {
    if (rules === "card") {
      loop.push(
        <Col md={3} xs={6} key={i} className="mb-4">
          <div className="wrapper">
            <div className="card-loader card-loader--tabs"></div>
          </div>
        </Col>
      );
    } else if (rules === "list") {
      loop.push(
        <Col md={12} xs={12} key={i} className="mb-2">
          <div className="wrapper">
            <div className="list-loader list-loader--tabs"></div>
          </div>
        </Col>
      );
    }
  }
  return <Row>{loop}</Row>;
};

export default Loads;
