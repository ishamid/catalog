import { Row, Col, Card } from "react-bootstrap";
import Loads from "../components/Loads";

const Content = ({ bind, commit, load }) => {
  return (
    <div>
      <Row className="part-content">
        <Col xs={12} md={12}>
          <h3>{commit}</h3>
        </Col>
      </Row>

      {load ? (
        <Loads tiles={8} rules="card" />
      ) : (
        <Row className="part-content">
          {bind.map((item, idx) => (
            <Col md={3} xs={6} key={idx} className="mb-4">
              <Card className="card-custom">
                <Card.Img
                  variant="top"
                  className="img"
                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYFjH74VxoAz2u1Wgu4ZCO8TM2ouz8Xso26Q&usqp=CAU"
                />
                <Card.Body className="body">
                  <Card.Title className="head">{item.sku}</Card.Title>
                  <Card.Text className="desc">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      )}
    </div>
  );
};

export default Content;
