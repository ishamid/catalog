import { Accordion, ListGroup } from "react-bootstrap";
import Loads from "../components/Loads";

const Categories = ({ categorys, selected, load }) => {
  const loadChilds = (data) => {
    if (data.children_data.length !== 0) {
      for (let i = 0; i < data.children_data.length; i++) {
        return data.children_data.map((child, idx) =>
          child.children_data.length !== 0 ? (
            <Accordion key={idx}>
              <Accordion.Item className="acor_custom_item" eventKey={child}>
                <Accordion.Header className="acor_custom_header">
                  {child.name}
                </Accordion.Header>
                <Accordion.Body className="acor_custom_body">
                  {loadChilds(child)}
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          ) : (
            <ListGroup className="list_custom" key={idx}>
              <ListGroup.Item
                className="list_custom_item"
                onClick={selected.bind(this, child)}
              >
                {child.name}
              </ListGroup.Item>
            </ListGroup>
          )
        );
      }
    }
  };

  return (
    <div>
      {load ? (
        <Loads tiles={8} rules="list" />
      ) : (
        <div>
          {categorys.map((data, idx) =>
            data.children_data.length !== 0 ? (
              <Accordion key={idx} className="parent">
                <Accordion.Item className="acor_custom_item" eventKey={data.id}>
                  <Accordion.Header className="acor_custom_header">
                    {data.name}
                  </Accordion.Header>
                  <Accordion.Body className="acor_custom_body">
                    {loadChilds(data)}
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
            ) : (
              <ListGroup className="list_custom parent" key={idx}>
                <ListGroup.Item
                  className="list_custom_item"
                  onClick={selected.bind(this, data)}
                >
                  {data.name}
                </ListGroup.Item>
              </ListGroup>
            )
          )}
        </div>
      )}
    </div>
  );
};

export default Categories;
