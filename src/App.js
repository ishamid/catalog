import React, { Component } from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import { Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

const Home = React.lazy(() => import("./views/Homepage"));

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <BrowserRouter>
        <Container fluid>
          <React.Suspense fallback={loading()}>
            <Switch>
              <Route
                path="*"
                name="Home"
                render={(props) => <Home {...props} />}
              />
            </Switch>
          </React.Suspense>
        </Container>
      </BrowserRouter>
    );
  }
}

export default App;
