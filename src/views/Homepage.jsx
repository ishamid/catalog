import { useState, useEffect } from "react";
import { getCategory, getProduct } from "../services/Global";
import Categories from "../partials/Categories";
import Content from "../partials/Content";
import { Navbar, Row, Col, Offcanvas, Button, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThList } from "@fortawesome/free-solid-svg-icons";

function Homepage() {
  const [categorys, setCategorys] = useState([]);
  const [product, setProduct] = useState([]);
  const [commit, setCommit] = useState([]);
  const [load, setLoad] = useState([]);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    const getCategorys = async () => {
      await setLoad(true);
      const categorysFromServer = await getCategory();
      setCategorys(categorysFromServer);
      setLoad(false);
    };

    getProductsDefault();
    getCategorys();
  }, []);

  const getProductsDefault = async () => {
    await setLoad(true);
    const defaultData = await getProduct(2);
    setProduct(defaultData);
    setCommit("Product");
    setLoad(false);
  };

  const handler = (param) => {
    getProducts(param.id);
    setCommit(param.name);
  };

  const getProducts = async (id) => {
    const ProductsFromServer = await getProduct(id);
    setProduct(ProductsFromServer);
  };

  const dataChange = (e) => {
    const type = e.target.value.toLowerCase();
    console.log(type);

    if (type.length > 2) {
      const filtered = product.filter((dd) =>
        dd.sku.toLowerCase().includes(type)
      );
      setProduct(filtered);
    } else if (type.length === 0) {
      getProductsDefault();
    }
  };

  return (
    <>
      <Navbar sticky="top" variant="light" className="bg-custom">
        <Navbar.Brand href="#home" className="brand">
          {window.innerWidth < 768 ? (
            <span onClick={handleShow}>
              <FontAwesomeIcon className="toggleMenu" icon={faThList} />
            </span>
          ) : (
            ""
          )}
          #fanscatalog
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Form className="d-flex">
            <Form.Control
              size="sm"
              type="text"
              placeholder="Search"
              className="me-2"
              onKeyUp={dataChange}
            />
            <Button size="sm" variant="outline-primary">
              login
            </Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>

      <Offcanvas show={show} onHide={handleClose}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Categories</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Categories
            categorys={categorys}
            selected={handler}
            load={load}
          ></Categories>
        </Offcanvas.Body>
      </Offcanvas>

      <Row className="pt-3">
        {window.innerWidth < 768 ? (
          <div></div>
        ) : (
          <Col md={2}>
            <Categories
              categorys={categorys}
              selected={handler}
              load={load}
            ></Categories>
          </Col>
        )}

        <Col xs={12} md={10}>
          <Content bind={product} commit={commit} load={load}></Content>
        </Col>
      </Row>
    </>
  );
}

export default Homepage;
